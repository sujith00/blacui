# GideonsWeb

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## To build the layout 
Step 01: npm install -g sass

Step 02: Run the following commands to generate the css.

sass  src/assets/layout/css/layout-blue-grey.scss:src/assets/layout/css/layout-blue-grey.css
sass  src/assets/layout/css/layout-blue-orange.scss:src/assets/layout/css/layout-blue-orange.css
sass  src/assets/layout/css/layout-cyan-deeporange.scss:src/assets/layout/css/layout-cyan-deeporange.css
sass  src/assets/layout/css/layout-darkpink-cyan.scss:src/assets/layout/css/layout-darkpink-cyan.css
sass  src/assets/layout/css/layout-deeppurple-teal.scss:src/assets/layout/css/layout-deeppurple-teal.css
sass  src/assets/layout/css/layout-green-orange.scss:src/assets/layout/css/layout-green-orange.css
sass  src/assets/layout/css/layout-green-pink.scss:src/assets/layout/css/layout-green-pink.css
sass  src/assets/layout/css/layout-green-purple.scss:src/assets/layout/css/layout-green-purple.css
sass  src/assets/layout/css/layout-indigo-purple.scss:src/assets/layout/css/layout-indigo-purple.css
sass  src/assets/layout/css/layout-indigo-yellow.scss:src/assets/layout/css/layout-indigo-yellow.css
sass  src/assets/layout/css/layout-orange-cyan.scss:src/assets/layout/css/layout-orange-cyan.css
sass  src/assets/layout/css/layout-orange-indigo.scss:src/assets/layout/css/layout-orange-indigo.css
sass  src/assets/layout/css/layout-pink-cyan.scss:src/assets/layout/css/layout-pink-cyan.css
sass  src/assets/layout/css/layout-pink-teal.scss:src/assets/layout/css/layout-pink-teal.css
sass  src/assets/layout/css/layout-teal-yellow.scss:src/assets/layout/css/layout-teal-yellow.css
