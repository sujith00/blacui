import { TabFormViewModel } from 'src/app/shared/models/common/tab-form-view.model';
import { Accessibility } from 'src/app/configuration/models/accessibility.model';

export class StandardConfigurationViewModel extends TabFormViewModel {

    AccessibilityList: Accessibility[];
    TabIndex: number; 
    ActionOrWidgetLinkedWith: string;

    constructor() {
        super(); 
        this.TabIndex = 5;      
    }      
}
