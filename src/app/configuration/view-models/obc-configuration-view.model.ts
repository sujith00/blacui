// Models
import { TabFormViewModel } from 'src/app/shared/models/common/tab-form-view.model';
import { Accessibility } from 'src/app/configuration/models/accessibility.model';

export class ObcConfigurationViewModel extends TabFormViewModel {
    AccessibilityList: Accessibility[];
    constructor() {
        super();
    }
}
