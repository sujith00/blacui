import { TestBed } from '@angular/core/testing';

import { ConfigurationCoreService } from './configuration.service';

describe('ConfigurationCoreService', () => {
  let service: ConfigurationCoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfigurationCoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
