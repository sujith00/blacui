import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of } from 'rxjs';

import { BaseHttp } from 'src/app/core/base-http';
import { Accessibility } from '../models/accessibility.model';
import { BlacException } from 'src/app/shared/models/common/blac-exception.model';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService extends BaseHttp{

  constructor(http: HttpClient) {
    super(http);
  }

  GetAccessibilities(): Observable<Array<Accessibility> | BlacException> {
      return this.HttpGet<Accessibility[]>('configuration/accessibilities');
  }
  
}
