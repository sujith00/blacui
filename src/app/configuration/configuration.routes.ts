import { Routes, } from '@angular/router';

import { StandardConfigurationComponent } from './standard-configuration/standard-configuration.component';
import { ObcConfigurationComponent } from './obc-configuration/obc-configuration.component';

export const configurationRoutes: Routes = [
    { path: 'standard-configuration', component: StandardConfigurationComponent },
    { path: 'obc-configuration', component: ObcConfigurationComponent }
];
