import { Router } from '@angular/router';
import { Component, ViewEncapsulation } from '@angular/core';

import { BaseComponent } from 'src/app/shared/base/base.component';
import { RouteManagerService } from 'src/app/core/services/route-manager.service';
import { ActionPanelItemService } from 'src/app/shared/services/action-panel-item.service';
import { Constant } from 'src/app/shared/utility/constant';
import { ConfigurationService } from '../services/configuration.service';
import { Accessibility } from '../models/accessibility.model';
import { BlacException } from 'src/app/shared/models/common/blac-exception.model';
import { StandardConfigurationViewModel } from '../view-models/standard-configuration-view.model';
import { TranslateItemService } from 'src/app/shared/services/translate-item.service';


@Component({
  selector: 'app-standard-configuration',
  templateUrl: './standard-configuration.component.html',
  styleUrls: ['./standard-configuration.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class StandardConfigurationComponent extends BaseComponent<StandardConfigurationViewModel> {

  constructor(public routeManagerService: RouteManagerService,
    public router: Router,
    private actionPanelItemService: ActionPanelItemService,
    private configurationService: ConfigurationService,
    private translateItemService: TranslateItemService) {
    super(routeManagerService, router, new StandardConfigurationViewModel());
    this.Title = this.translateItemService.getTranslateMessageValue('menu.standard_configuration');
    this.FormViewModel.ActionOrWidgetLinkedWith = Constant.ActionOrWidgetLinkedWith.ACCESSIBILITY;
    this.populateAcionPanelItems();
  }

  async moduleOnInit(): Promise<void> {
    this.configurationService.GetAccessibilities().subscribe(
      (results: Accessibility[]) => {
        this.FormViewModel.AccessibilityList = results;
      },
      (err: BlacException) => {
        console.log(err.Message);
      },
      () => {
      });
  }

  protected populateAcionPanelItems(): void {
    if (this.FormViewModel.ActionOrWidgetLinkedWith !== '') {
      this.actionPanelItemService.getActionalPanelList(this.FormViewModel.ActionOrWidgetLinkedWith).subscribe(
        (data) => { this.ActionPanelItems = data; }
      );
    }
  }

  changeTab(index: number): void {
    this.FormViewModel.TabIndex = index;
  }
}