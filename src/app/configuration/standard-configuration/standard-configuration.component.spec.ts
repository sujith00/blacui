import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardConfigurationComponent } from './standard-configuration.component';

describe('StandardComponent', () => {
  let component: StandardConfigurationComponent;
  let fixture: ComponentFixture<StandardConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
