import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { BaseComponent } from 'src/app/shared/base/base.component';
import { RouteManagerService } from 'src/app/core/services/route-manager.service';
import { TranslateItemService } from 'src/app/shared/services/translate-item.service';
import { ObcConfigurationViewModel } from '../view-models/obc-configuration-view.model';

@Component({
  selector: 'app-obc-configuration',
  templateUrl: './obc-configuration.component.html',
  styleUrls: ['./obc-configuration.component.css']
})

export class ObcConfigurationComponent extends BaseComponent<ObcConfigurationViewModel> {
  form1= false;
  form2= false;
  constructor(public routeManagerService: RouteManagerService,
    public router: Router,
    private translateItemService: TranslateItemService) {
    super(routeManagerService, router, new ObcConfigurationViewModel());
    this.Title = this.translateItemService.getTranslateMessageValue('menu.obc_configuration');
  }
}