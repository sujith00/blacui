import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObcConfigurationComponent } from './obc-configuration.component';

describe('ObcComponent', () => {
  let component: ObcConfigurationComponent;
  let fixture: ComponentFixture<ObcConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObcConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObcConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
