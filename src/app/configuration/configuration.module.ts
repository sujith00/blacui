import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { configurationRoutes } from './configuration.routes';
import { StandardConfigurationComponent } from './standard-configuration/standard-configuration.component';
import { ObcConfigurationComponent } from './obc-configuration/obc-configuration.component';

@NgModule({
  declarations: [StandardConfigurationComponent, ObcConfigurationComponent],
  imports: [
    FormsModule,
    SharedModule,
    RouterModule.forChild(configurationRoutes)
  ]
})
export class ConfigurationModule { }
