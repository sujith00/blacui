
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { ScheduleComponent } from './schedule/schedule.component';


export const ftcRoutes: Routes = [
  { path: 'schedule', component: ScheduleComponent }
];
@NgModule({
  declarations: [ScheduleComponent],
  imports: [
    FormsModule,
    SharedModule,
    RouterModule.forChild(ftcRoutes)
  ]
})
export class  TripSchedulerModule { }
{ }

