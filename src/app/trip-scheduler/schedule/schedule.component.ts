import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { RouteManagerService } from 'src/app/core/services/route-manager.service';
import { Router } from '@angular/router';
import { ActionPanelItemService } from 'src/app/shared/services/action-panel-item.service';
import { SchedulerViewModel } from '../scheduler-view.model';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent extends BaseComponent<SchedulerViewModel> {

  public currentStep = 2;
  Filtering: boolean;
  Preparations: boolean;
  Schdueling: boolean;


  public steps = [
    { label: 'Personal Info', isValid: this.isStepValid },
    { label: 'Education', isValid: this.isStepValid },
    { label: 'Experience', isValid: this.isStepValid },
    { label: 'Attachments', isValid: this.isStepValid, optional: true }
  ];

  constructor(public routeManagerService: RouteManagerService,
    public router: Router,
    private actionPanelItemService: ActionPanelItemService) {
    super(routeManagerService, router, new SchedulerViewModel());
    this.Title = "Schedule";
    this.Filtering = true;
  }



  public isStepValid(index: number): boolean {
    return index % 2 === 0;
  }
  filterClick(): void {
    this.Filtering = true;
    this.Preparations = false;
    this.Schdueling = false;
  }

  preparationsClick(): void {
    this.Filtering = false;
    this.Preparations = true;
    this.Schdueling = false;
  }

  SchedulingClick(): void {
    this.Filtering = false;
    this.Preparations = false;
    this.Schdueling = true;
  }

}
