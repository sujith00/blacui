import { TabFormViewModel } from 'src/app/shared/models/common/tab-form-view.model';
export class SchedulerViewModel extends TabFormViewModel {

       TabIndex: number; 
    ActionOrWidgetLinkedWith: string;

    constructor() {
        super(); 
        this.TabIndex = 5;      
    }      
}
