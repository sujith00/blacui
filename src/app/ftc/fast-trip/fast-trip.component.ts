import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../shared/base/base.component';
import { StandardConfigurationViewModel } from '../../configuration/view-models/standard-configuration-view.model';
import { RouteManagerService } from '../../core/services/route-manager.service';
import { Router } from '@angular/router';
import { ActionPanelItemService } from '../../shared/services/action-panel-item.service';
import { Constant } from '../../shared/utility/constant';

@Component({
  selector: 'app-fast-trip',
  templateUrl: './fast-trip.component.html',
  styleUrls: ['./fast-trip.component.css']
})
export class FastTripComponent extends BaseComponent<StandardConfigurationViewModel> {
  public data: any[] = [{
    kind: 'Active', share: 0.118
   }, {

    kind: 'Committed', share: 0.175
    }, {
    kind: 'Confirm', share: 0.238
    }, {
   
    kind: 'Cancel', share: 0.052
    } ];
  constructor(public routeManagerService: RouteManagerService,
    public router: Router,
    private actionPanelItemService: ActionPanelItemService) {
    super(routeManagerService, router, new StandardConfigurationViewModel());
    this.Title = "Fast Trip";
    this.FormViewModel.ActionOrWidgetLinkedWith = Constant.ActionOrWidgetLinkedWith.ACCESSIBILITY;
   
  }

}


