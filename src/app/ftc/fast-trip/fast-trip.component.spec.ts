import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FastTripComponent } from './fast-trip.component';

describe('FastTripComponent', () => {
  let component: FastTripComponent;
  let fixture: ComponentFixture<FastTripComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FastTripComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FastTripComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
