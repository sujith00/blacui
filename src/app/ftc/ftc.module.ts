import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import {FastTripComponent } from './fast-trip/fast-trip.component';
import { Routes, } from '@angular/router';
export const ftcRoutes: Routes = [
    { path: 'fast-trip', component: FastTripComponent }
];
@NgModule({
  declarations: [FastTripComponent],
  imports: [
    FormsModule,
    SharedModule,
    RouterModule.forChild(ftcRoutes)
  ]
})
export class FastTripModule { }
