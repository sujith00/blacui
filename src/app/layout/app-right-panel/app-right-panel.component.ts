import {Component, ViewChild, AfterViewInit} from '@angular/core';

import { AppComponent } from 'src/app/app.component';
import { ScrollPanelComponent } from '../scroll-panel/scroll-panel.component';

@Component({
  selector: 'app-right-panel',
  templateUrl: './app-right-panel.component.html',
  styleUrls: ['./app-right-panel.component.css']
})
export class AppRightPanelComponent implements AfterViewInit {

  @ViewChild('scrollRightPanel',{static: false}) rightPanelMenuScrollerViewChild: ScrollPanelComponent;

  constructor(public app: AppComponent) {}

  ngAfterViewInit() {
      setTimeout(() => {this.rightPanelMenuScrollerViewChild.moveBar(); }, 100);
  }

  onTabChange(event) {
      setTimeout(() => {this.rightPanelMenuScrollerViewChild.moveBar(); }, 450);
  }

}
