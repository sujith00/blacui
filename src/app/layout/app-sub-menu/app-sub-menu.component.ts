import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { AppComponent } from 'src/app/app.component';
import { AppMenuComponent } from '../app-menu/app-menu.component';
import { MenuItem } from '../models/app-menu.model';

@Component({
  /* tslint:disable:component-selector */
  selector: '[app-submenu]',
  /* tslint:enable:component-selector */
  templateUrl: './app-sub-menu.component.html',
  styleUrls: ['./app-sub-menu.component.css'],
  animations: [
      trigger('children', [
          state('hiddenAnimated', style({
              height: '0px'
          })),
          state('visibleAnimated', style({
              height: '*'
          })),
          state('visible', style({
              height: '*',
              'z-index': 100
          })),
          state('hidden', style({
              height: '0px',
              'z-index': '*'
          })),
          transition('visibleAnimated => hiddenAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
          transition('hiddenAnimated => visibleAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
      ])
  ]
})
export class AppSubMenuComponent {

  @Input() item: MenuItem;
  @Input() root: boolean;
  @Input() visible: boolean;

  ParentActive: boolean;
  Reset: boolean;
  activeIndex: number;

  constructor(public app: AppComponent, public appMenu: AppMenuComponent) { }

  itemClick(event: Event, item: MenuItem, index: number) {
      if (this.root) {
          this.app.menuHoverActive = !this.app.menuHoverActive;
      }
      // avoid processing disabled items
      if (item.disabled) {
          event.preventDefault();
          return true;
      }

      // activate current item and deactivate active sibling if any
      this.activeIndex = (this.activeIndex === index) ? null : index;

      // execute command
      if (item.command) {
          item.command({ originalEvent: event, item: item });
      }

      // prevent hash change
      if (item.items || (!item.url && !item.routerLink)) {
          setTimeout(() => {
              this.appMenu.layoutMenuScrollerViewChild.moveBar();
          }, 450);
          event.preventDefault();
      }

      // hide menu
      if (!item.items) {
          if (this.app.isHorizontal()) {
              this.app.resetMenu = true;
          } else {
              this.app.resetMenu = false;
          }

          this.app.overlayMenuActive = false;
          this.app.staticMenuMobileActive = false;
          this.app.menuHoverActive = !this.app.menuHoverActive;
      }
  }

  onMouseEnter(index: number) {
      if (this.root && this.app.menuHoverActive && (this.app.isHorizontal())
          && !this.app.isMobile() && !this.app.isTablet()) {
          this.activeIndex = index;
      }
  }

  isActive(index: number): boolean {
      return this.activeIndex === index;
  }

  @Input() get reset(): boolean {
      return this.Reset;
  }

  set reset(val: boolean) {
      this.Reset = val;

      if (this.Reset && (this.app.isHorizontal())) {
          this.activeIndex = null;
      }
  }

  @Input() get parentActive(): boolean {
      return this.ParentActive;
  }

  set parentActive(val: boolean) {
      this.ParentActive = val;

      if (!this.ParentActive) {
          this.activeIndex = null;
      }
  }
}
