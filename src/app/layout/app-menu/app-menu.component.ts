import { Component, Input, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { ScrollPanelComponent } from '../scroll-panel/scroll-panel.component';
import { RouteConstant } from 'src/app/shared/route-constant';

@Component({
    selector: 'app-menu',
    templateUrl: './app-menu.component.html'
})
export class AppMenuComponent implements OnInit, AfterViewInit {

    @Input() reset: boolean;
    @ViewChild('layoutMenuScroller', {static: false}) layoutMenuScrollerViewChild: ScrollPanelComponent;
    model: any[];

    constructor(public app: AppComponent) {
        this.model = [];
     }

    ngOnInit() {
       this.addConfigurationMenus();
    }

    ngAfterViewInit() {
        setTimeout(() => { this.layoutMenuScrollerViewChild.moveBar(); }, 100);
    }

 
    addConfigurationMenus(): void {
        this.model.push(
            {
                label: 'Configuration', icon: 'fa fa-fw icon-working',
                items: [
                    { label: 'Standard Configuration', icon: 'fa fa-fw icon-reoconciliation', routerLink: [RouteConstant.CONFIGURATION_STANDARD]  },
                    { label: 'OBC Configuration', icon: 'fa fa-fw icon-test-supervions-1', routerLink: [RouteConstant.CONFIGURATION_OBC]  }
                ]
            }
        );
        this.model.push(
            {
                label: 'Resources', icon: 'fa fa-fw icon-working',
                items: [
                    { label: 'Logistics', icon: 'fa fa-fw icon-reoconciliation', routerLink: [RouteConstant.RESOURCES_LOGISTICS]  },
                    { label: 'Installation', icon: 'fa fa-fw icon-test-supervions-1', routerLink: [RouteConstant.RESOURCES_INSTALLATION]  },
                    { label: 'Sources', icon: 'fa fa-fw icon-reoconciliation', routerLink: [RouteConstant.RESOURCES_SOURCES]  }
                ]
            }
        );
        this.model.push(
            {
                label: 'Fast Trip Confirmation', icon: 'fa fa-fw icon-working', routerLink: [RouteConstant.FAST_TRIP]
             }
        );

        this.model.push(
            {
                label: 'Trip Scheduler', icon: 'fa fa-fw icon-working', routerLink: [RouteConstant.TRIP_SCHEDULER]
             }
        );
    }

    onMenuClick(event) {
        if (!this.app.isHorizontal()) {
            setTimeout(() => {
                this.layoutMenuScrollerViewChild.moveBar();
            }, 450);
        }

        this.app.onMenuClick(event);
    }
}
