import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActionPanelItem } from '../../models/action-panel-item.model';


@Component({
  selector: 'app-action-panel',
  templateUrl: './action-panel.component.html',
  styleUrls: ['./action-panel.component.css']
})
export class ActionPanelComponent implements OnInit {

  @Input() ActionPanelItems: Array<ActionPanelItem>;
  @Output() ActionPanelItemsClicked = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  onActionPanelItemsClicked(actionPanelItemKey: string): void {
    this.ActionPanelItemsClicked.emit(actionPanelItemKey);
  }

}
