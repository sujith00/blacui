export class Tab {
    id: number;
    title: string;
    url: string;
    bundle: any;
    bundleArray: [string, any][];
}
