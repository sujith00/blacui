export class ActionPanelItem {
    ActionPanelItemKey: string;
    ActionPanelItemLinkedWith: Array<string>;
    FunctionCode: string;
    Description: string;
    Icon: string;
    Disabled: boolean;
    IsMultiSelectionAllowed: boolean;
    command?: (event?: any) => void;

    constructor() {
        this.ActionPanelItemKey = '';
        this.ActionPanelItemLinkedWith = new Array<string>();
        this.FunctionCode = '';
        this.Description = '';
        this.Icon = '';
        this.Disabled = false;
        this.IsMultiSelectionAllowed = false;
    }

    onSingleSelect(): void {
        this.Disabled = false;
    }

    onMultiselect(): void {
        if (this.IsMultiSelectionAllowed) {
            this.Disabled = false;
        } else {
            this.Disabled = true;
        }
    }

    disable(): void{
        this.Disabled = true;
    }

    enable(): void{
        this.Disabled = false;
    }
}
