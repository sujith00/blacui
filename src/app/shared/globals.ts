export let Countries = {
  Australia : 'AUS',
  Singapore : 'SIN',
  Taiwan    : 'TWN',
  India     : 'IND'
};

export let CountryMapZoom = {
  Australia : 7,
  Singapore : 12,
  Taiwan    : 9,
  India     : 10
};

export let Country = Countries.Australia;

export let CountryMapZoomVal = CountryMapZoom.Australia;

export interface Global {
  InputDateFormat: string,
    InputDateTimeFormat: string,
    DateTimeFormatMarker: string,
    UseTabView: boolean,
    InputCalenderDateFormat: string,
    PageNo: number,
    ItemsPerPage: number,
    ShowTheme: boolean,
    MapZoomVal: number,
    Version:string,
    Apibase: string,
    UseMock:boolean
}


export let GlobalVariable: Global = {   
    InputDateFormat: 'yyyy-MM-dd',
    InputDateTimeFormat: 'yyyy-MM-dd HH:mm',
    DateTimeFormatMarker: 'yyyy-MM-dd h:mm a',
    UseTabView: true,
    InputCalenderDateFormat: 'dd/mm/yy',
    PageNo: 1,
    ItemsPerPage: 15,
    ShowTheme: false,
    MapZoomVal: CountryMapZoomVal,
    Version: '2.0.1',
    Apibase: 'http://localhost:4200/',
    UseMock:true
};
