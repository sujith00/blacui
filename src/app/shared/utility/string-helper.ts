export class StringHelper {

    static Concat(...args): string {
        let stringBuilder = [];
         args.forEach((item) => {
            stringBuilder.push(item);
        })       

        return stringBuilder.join('');
    }

}