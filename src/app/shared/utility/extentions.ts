export class Extentions {
    static ToValidDateTime(date: Date): string {
        if (date == null) {
            const d = new Date();
            const dArray = d.toLocaleDateString('en-GB').split('/');
            return dArray[2] + '-' +  dArray[1] + '-' +  dArray[0] + ' ' + d.toLocaleTimeString();
        } else {
            const dArray = date.toLocaleDateString('en-GB').split('/');
            return dArray[2] + '-' +  dArray[1] + '-' +  dArray[0] + ' ' + date.toLocaleTimeString();
        }
    }

    static IsDate(value: any){
        return false;
    }

}


export class ObjectExtentions {}