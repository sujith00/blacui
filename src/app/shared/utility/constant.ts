export class Constant {
    static FunctionCode = {
        // Product
        FC_MNU_CREATE_PRODUCT: 'mnuCreateProduct',
        FC_MNU_PRODUCT_SUPERVISION: 'mnuProductSupervision',
        FC_MNU_PRODUCT_AVAILABILITY: 'mnuProductAvailability',
        FC_MNU_NATIONAL_PRICE_LIST: 'mnuNationalPriceList',
        // Customer
        FC_MNU_CREATE_CUSTOMER: 'mnuCreateCustomer',
        FC_MNU_CUSTOMER_SUPERVISION: 'mnuCustomerSupervision',
        FC_MNU_CUSTOMER_ENQUIRY: 'mnuCustomerEnquiry',
        FC_MNU_CUSTOMERS: 'mnuCustomers',
        // Orders
        FC_MNU_CREATE_DELIVERY_ORDER: 'mnuCreateDeliveryOrder',
        FC_MNU_DELIVERY_ORDER_SUPERVISION: 'mnuDeliveryOrderSupervision',
        // Finance
        FC_MNU_CREDITS_AND_COLLECTION: 'mnuCreditsAndCollection',
        FC_MNU_INVOICING: 'mnuInvoicing',
        FC_MNU_RECEIPTS: 'mnuReceipts',
        FC_MNU_ADJUSTMENTS_AND_JOURNALS: 'mnuAdjustmentsAndJournals',
        FC_MNU_COLLECTION_MANAGEMENT: 'mnuCollectionManagement',
        FC_MNU_CREATE_RECEIPT: 'mnuCreateReceipt',
        FC_MNU_TRANSACTION_MATCHING: 'mnuTransactionMatching',
        FC_MNU_BLOCK_CUSTOMER: 'mnuBlockCustomer',
        FC_MNU_UN_BLOCK_CUSTOMER: 'mnuUnBlockCustomer',
        FC_MNU_NEW_NOTE: 'mnuNewNote',
        FC_MNU_MODIFY_NOTE: 'mnuModifyNote',
        FC_MNU_UPDATE_ATB: 'mnuUpdateATB',
        FC_MNU_EMAIL_STATEMENT: 'mnuEmailStatement',
        FC_MNU_NEW_CREDIT_LIMIT: 'mnuNewCreditLimit',
        FC_MNU_MODIFY_CREDIT_LIMIT: 'mnuModifyCreditLimit',
        FC_MNU_BATCH_UPLOAD_CREDIT_LIMIT: 'mnuBatchUploadCreditLimit',
        FC_MNU_COLL_MGT_TASK_EMAIL: 'mnuCollectionMangementTaskEmail',
        FC_MNU_EXPORT_REMINDERS: 'mnuExportReminders',
        FC_MNU_EMAIL_REMINDERS: 'mnuEmailReminders',
        FC_MNU_PRINT_REMINDERS: 'mnuPrintReminders',
        FC_MNU_BANKING_SLIP: 'mnuBankingSlip',
        FC_MNU_BANK_DEPOSIT: 'mnuBankDeposit',
        FC_MNU_CREDIT_NOTE_WORKFLOW: 'mnuCreditNoteWorkflow',
        FC_MNU_WRITE_OFF_TRANSACTIONS: 'mnuWriteOffTransactions',
        FC_MNU_UNMATCH_TRANSACTIONS: 'mnuUnMatchTransactions',
        FC_MNU_TRANSACTION_ENQUIRY: 'mnuTransactionEnquiry',
        FC_MNU_AUTO_TRANSACTION_MATCHING: 'mnuAutoTransactionMatching',
        // Maintenance
        FC_MNU_DO_OVERRIDE: 'mnuDoOverride',
        FC_MNU_JOURNAL_ENQUIRY: 'mnuJournalEnquiry',

        // Agreement
        FC_MNU_AGREEMENT_CREATE: 'mnuAgreementCreate',
        FC_MNU_AGREEMENT_SURRENDER: 'mnuAgreementSurrender',
        FC_MNU_AGREEMENT_SWAP: 'mnuAgreementSwap',
        FC_MNU_AGREEMENT_HISTORY: 'mnuAgreementHistory',
        FC_MNU_AGREEMENTS: 'mnuAgreements',

        // application
        FC_MENU_CUSTOMER_APPLICATION: 'mnuCustApplication',

        FC_MENU_CUSTOMER_CYLINDERDEPOSIT: 'mnuCylinderDeposit',
        FC_MENU_CREDIT_ADJUSTMENT: 'mnuCreditAdjustment',

        // Contract
        FC_MNU_INDEXATION: 'test',
        FC_MNU_INDEX_CODE: 'mnuIndexCode',
        FC_MNU_RANGE: 'mnuRange',
        FC_MNU_INDEXATION_UPLOAD_VALIDATE: 'mnuDoOverride',
        FC_MNU_INDEXATION_UPLOAD_CLEAR: 'mnuDoOverride',
        FC_MNU_INDEXATION_UPLOAD_SAVE: 'mnuDoOverride',
        FC_MNU_SPECIAL_FORMULA: 'mnuSpecialFormula',
        FC_MNU_CONTRACT_LIST: 'mnuContractList',
        FC_MNU_CONTRACT_CUSTOMER: 'mnuContractCustomer',
        FC_MNU_CONTRACT_PRICE: 'mnuContractPrice',
        FC_MNU_SCADA_TAGS: 'mnuScadaTags',
        FC_MNU_CONSUMPTION_READING: 'mnuConsumptionReading',

        // SmartList
        FC_MENU_SMART_LIST: 'mnuSmartList',

        // Debit note and Credit note
        FC_COMBO_TAX_DEBIT_NOTE: 'cmbTax_DEBIT_NOTE',
        FC_COMBO_TAX_CREDIT_NOTE: 'cmbTax_CREDIT_NOTE',
        // Approvals
        FC_MENU_APPROVALS: 'mnuApprovals',
        FC_MENU_APPROVAL_WORKFLOW: 'mnuApprovalWorkflow'
    };

    static ActionOrWidgetLinkedWith = {
        ACCESSIBILITY: 'ACCESSIBILITY'
    };

    static ActionPanelItemKey = {
        KEY_AGREEMENT_CREATE: 'KEY_AGREEMENT_CREATE'

    };

    static Icon = {
        // Common
        IC_MODIFY: 'fa fa-pencil-square-o',
        IC_EMAIL: 'fa fa-envelope',
        IC_EXPORT_EXCEL: 'fa fa-file-excel-o',
        IC_PRINT: 'fa fa-print',
        // Product
        IC_CREATE_PRODUCT: 'fa fa-snowflake-o',
        IC_PRODUCT_AVAILABILITY: 'fa fa-briefcase',
        IC_NATIONAL_PRICE_LIST: 'fa fa-user-circle',
        // Customer
        IC_CUSTOMER_ENQUIRY: 'fa fa-address-book',
        IC_INVOICING: 'fa fa-money',
        IC_VIEW_RECEIPTS: 'fa fa-ticket',
        IC_VIEW_DEBIT_NOTE: 'fa fa-ticket',
        IC_VIEW_CREDIT_NOTE: 'fa fa-ticket',
        // Finanacefa
        IC_OPEN_ACTIVITIES: 'fa fa-folder-open',
        IC_OPEN_CASES: 'fa fa-briefcase',
        IC_CREDIT_LIMIT_EXCEEDED: 'fa fa-exclamation-triangle',
        IC_UN_MATCHED_INVOICES: 'fa fa-hand-pointer-o',
        IC_UN_MATCHED_RECEIPTS: 'fa fa-hand-pointer-o',
        IC_COLLECTION_MANAGEMENT: 'fa fa-copyright',
        IC_CREATE_RECEIPT: 'fa fa-ticket',
        IC_TRANSACTION_MATCHING: 'fa fa-exchange',
        IC_BLOCK_CUSTOMER: 'fa fa-ban',
        IC_UN_BLOCK_CUSTOMER: 'fa fa-folder-open-o',
        IC_NEW_NOTE: 'fa fa-commenting',
        IC_UPDATE_ATB: 'fa fa-calculator',
        IC_NEW_CREDIT_LIMIT: 'fa fa-credit-card',
        IC_BATCH_UPLOAD_CREDIT_LIMIT: 'fa fa-upload',
        IC_OVER_DUE: 'fa fa-exclamation-triangle',
        IC_PROMISE_TO_PAY: 'fa fa-money',
        IC_DISPUTE_INVOICES: 'fa fa-file-text',
        IC_UNCONFIRM: 'fa fa-ban',
        IC_CONFIRM: 'fa fa-check-circle',
        IC_SHIPMENT_INVOICE: 'fa fa-file-text',
        IC_UPDATE: 'fa fa-refresh',
        IC_UNBANKED_RECEIPTS: 'fa fa-money',
        IC_BANKED_RECEIPTS: 'fa fa-university',
        IC_OUTSTANDING_INVOICES: 'fa fa-list-alt',
        IC_MODIFY_RECEIPT: 'fa fa-ticket',
        IC_DELETE_RECEIPT: 'fa fa-trash-o',
        IC_BANKING_SLIP: 'fa fa-file-text-o',
        IC_UPDATE_RECEIPT: 'fa fa-refresh',
        IC_UNMATCH: 'fa fa-refresh',
        IC_VOID: 'fa fa-refresh',
        IC_BANK_DEPOSIT: 'fa fa-refresh',
        IC_CREATE_NEW_NOTE: 'fa fa-sticky-note-o',
        IC_NOTE_TASKS: 'fa fa-commenting',
        IC_EDIT_NOTE: 'fa fa-pencil',
        IC_CYLINDER_HOLDINGS_ENQUIRY: 'fa fa-book',
        IC_DO_ENQUIRY: 'fa fa-book',
        IC_AGREEMENT_CREATE: 'fa fa-ticket',
        IC_AGREEMENT_SURRENDER: 'fa fa-ticket',
        IC_AGREEMENT_SWAP: 'fa fa-ticket',
        IC_AGREEMENT_HISTORY: 'fa fa-ticket',
        IC_CUSTOMER_APPLICATION: 'fa fa-ticket',
        IC_CYLINDER_DEPOSIT: 'fa fa-ticket',
        IC_CREDIT_ADJUSTMENT: 'fa fa-ticket',
        IC_DASHBOARD: 'fa fa-credit-card',
        IC_CREATE: 'fa fa-plus',
        IC_COPY: 'fa fa-copy',
        // Contract
        IC_HISTORY: 'fa fa-history',
        IC_SAVE: 'fa fa-floppy-o',
        IC_APPROVE: 'fa fa-thumbs-o-up',
        IC_REJECT: 'fa fa-thumbs-o-down',
        IC_INTERNAL_COMMENTS: 'fa fa-comments-o'
    };

    static General = {
        YES: 'Y',
        NO: 'N',
        ACTIVE: 'A'
    };

    static Country = {
        Australia: 'AUS',
        Singapore: 'SIN',
        Taiwan: 'TWN',
        India: 'IND'
    };

    static MapZoom = {
        Australia: 7,
        Singapore: 12,
        Taiwan: 9,
        India: 10
    };

    static CustomerLevel = {
        NATIONAL: 0,
        ACCOUNT: 1,
        PARENT: 2,
        CHILD: 3
    };

    static CustomerLevelName = [
        'National',
        'Account',
        'Parent',
        'Child'
    ];

    static CustomerSearchType = {
        ACCOUNT: '0',
        PARENT: '1',
        CHILD: '2'
    };

    static CategoryFields = {
        CURRENT: 'Current',
        ZERO_TO_THIRTY: '0-30 Days',
        THIRTY_ONE_TO_SIXTY_DAYS: '31-60 Days',
        SIXTY_ONE_DAYS_NINTY_DAYS: '61-90 Days',
        MORE_THAN_NINTY_DAYS: '> 90 Days'
    };

    static ResponseStatus = {
        NOTE_NOT_FOUND: 1,
        OK: 200,
        CREATED: 201,
        NO_CONTENT: 204,
        BAD_REQUEST: 400,
        NOT_FOUND: 404,
        INTERNAL_SERVER_ERROR: 500
    };

    static Mode = {
        CREATE: 0,
        UPDATE: 1
    };

    static DebtorType = {
        NATIONAL: 'N',
        ACCOUNT: 'A',
        PARENT: 'P',
        CHILD: 'C'
    };

    static CollectionManagementAction = {
        DISPUTE_REASON: 'DISPUTE REASON',
        PROMISE_TO_PAY: 'PROMISE TO PAY'
    };

    static StorageKey = {
        COUNTRY_SETTINGS: 'COUNTRY_SETTINGS',
        SYSTEM_SETTINGS: 'SYSTEM_SETTINGS',
        GROUP_DATA: 'GROUP_DATA',
        ACCESS: 'ACCESS',
        REDIRECT_ROUTE: 'REDIRECT_ROUTE',
        CURRENCY_ROUNDINGS: 'CURRENCY_ROUNDINGS',
    };

    static StorageOption = {
        PERMANENT: 'PERMANENT',
        TEMPORARY: 'TEMPORARY'
    };

    static RoundingType = {
        DEFAULT: 'DF',
        EXCHANGE: 'EX',
        INVOICE_LINE: 'IL',
        QUANTITY: 'QT',
        TAX: 'TX'
    };

    static RoundSenario = {
        DEFAULT: 'D',
        TRUNCATE: 'T',
        SPECIAL: 'U'
    };

    static InvoiceOverrideRawCallbackCase = {
        INVOICE_HAS_EXCEEDED_THE_AMOUNT_SET_IN_COUNTRY_SETTINGS: '01',
        CUSTOMER_FIRST_INVOICE_FOR_THIS_INVOICE_TYPE: '02'
    };

    static InvoiceOverrideRawCallbackReturn = {
        INVOICE_HAS_EXCEEDED_THE_AMOUNT_SET_IN_COUNTRY_SETTINGS: 'invoice-exceeded-invoices',
        CUSTOMER_FIRST_INVOICE_FOR_THIS_INVOICE_TYPE: 'first-invoice-invoices',
        NATIONAL_CUSTOMER_IS_PUT_ON_HOLD: 'on-hold-invoices'
    };

    static InvoiceRawCallbackCase = {
        UNMATCHED_INVOICES: 'U',
        MATCHED_INVOICES: 'M',
        VOID_INVOICES: 'V',
        READY_TO_MATCH_INVOICES: 'T'
    };

    static InvoiceRawCallbackReturn = {
        UNMATCHED_INVOICES: 'unmatched-invoices',
        MATCHED_INVOICES: 'matched-invoices',
        VOID_INVOICES: 'void-invoices',
        READY_TO_MATCH_INVOICES: 'ready-to-match-invoices'
    };

    static CollectionManagementTask = {
        NOT_COMPLETED_STATUS: '0',
        COMPLETED_STATUS: '1'
    };

    static Status = [
        { text: 'All', value: '2' },
        { text: 'UnCompleted', value: '0' },
        { text: 'Completed', value: '1' }
    ];

    static ReferenceInfo = {
        COLLECTION_MANAGEMENT_NOTES: '31',
        RECEIPT: '32',
        JOURNAL: '33',
        CREDIT_NOTE: '34',
        DEBIT_NOTE: '35',
        AR_STATEMENT_FROM_COLLECTION_MANAGEMENT: '36',
        REMINDER_LETTERS: '37',
        CONTRACT: '10'
    };

    static messageCommand = {
        yes: 'Yes',
        no: 'No',
        ok: 'OK'
    };

    static accountReceivableSystem = 'account-receivable-system';

    static ReceiptStatus = {
        BANKED: 'B',
        MATCHED: 'Y',
        NOT_MATCHED: 'N'
    };

    static JournalStatus = {
        MATCHED: 'Y',
        NOT_MATCHED: 'N'
    };

    static DebtorTypeForAddressSearch = {
        ACCOUNT: 0,
        NATIONAL: 1,
        PARENT: 2
    };

    static ReasonType = {
        DISPUTE: 1,
        PROMISED_TO_PAY: 2,
        OUTSTANDING: 0
    };

    static CustomerStatusCallbackReturn = {
        DORMANT_CUSTOMER: 'dormant-customer',
    };

    static ReceiptMatchedStatus = {
        NOT_MATCHED: 0,
        IS_MATCHED: 1
    };

    static CustomerStatus = {
        ACTIVE: 'Y',
        HOLD: 'N',
        INACTIVE: 'D'
    };

    static BankTypes = {
        CUSTOMER_BANK: 0,
        AIR_LIQUIDE_BANK: 1
    };

    static PaymentTypes = {
        CREDIT_CARD: 'CC',
        CHEQUE: 'CQ',
        LOCAL_CURRENCY: 'LC',
        PROMISSORY_NOTE: 'PN',
        ELECTRONIC_PROMISSORY_NOTE: 'EP',
        CASH: 'CA',
        OTHER: 'OT'
    };

    static TransactionType = {
        DEBIT: 'DR',
        CREDIR: 'CR',
        RECEIPT: 'RC',
        INVOICE: 'IN'
    };

    static TransactionTypeCallbackReturn = {
        DEBIT_ROW: 'debit-row',
        CREDIT_ROW: 'credit-row'
    };

    static TransactionMatchingRowCallbackReturn = {
        IS_ADDED: 'is-added',
    };
    static CurrencyCode = {
        DEFAULT: 'AUD'
    };

    static ReceiptValueStatus = {
        NOT_BANKED: '0',
        BANKED: '1',
        UNMATCHED: '2',
        POST_DATE: '3'
    };

    static JournalReasonType = {
        Grouptype: '23'
    };

    static SegmentCode = {
        JournalGACR: 'JOURNAL_GA_CR',
        JournalGADR: 'JOURNAL_GA_DR'
    };

    static CostCenterSegmentCode = {
        JournalCCCR: 'JOURNAL_CC_CR',
        JournalCCDR: 'JOURNAL_CC_DR'
    };

    static JournalType = {
        CreditJornal: 'CR',
        DebitJournal: 'DR',
        TypeOfDocument: 'IJ',
        InvoiceRecordStatus: 'T',
        DocumentMatchStatus: 'Y',
        TransactionType: 'CJ',
        TransactinTypeDebit: 'DJ',
        SendFlag: 'F',
        GeneralLedgerAccountTypeE: 'E',
        GeneralLedgerAccountTypeR: 'R'
    };

    static CustomerGridHeaderLabel = [
        { CUSTOMER_NO_LABEL: 'account_no', CUSTOMER_NAME_LABEL: 'account_name' },
        { CUSTOMER_NO_LABEL: 'parent_no', CUSTOMER_NAME_LABEL: 'parent_name' },
        { CUSTOMER_NO_LABEL: 'dp_no', CUSTOMER_NAME_LABEL: 'dp_name' }
    ];

    static ALL = 'All';

    static PaymentType = {
        PROMISORY_NOTE: 'PN',
        ELECTRONIC_PROMISORY_NOTE: 'EP'
    };

    static CountrySettings = {
        APPLICABLE: 'Y',
        NOT_APPLICABLE: 'N',
        S: 'S',
        D: 'D'
    };

    static DefaultExchangeRate = 1.0000000;

    static CustomerStatusDescription = {
        ACTIVE: 'Active',
        INACTIVE: 'InActive',
        HOLD: 'Hold',
        DORMANT: 'Dormant'
    };

    static InvoiceStatus = {
        INVOICE_MATCHING_IN_PROGRESS: 'T',
        UNMATCHED_INVOICES: 'U'
    };

    static PurchaseOrderSearchByItemValues = {
        CUSTOMER_NAME: 0,
        CUSTOMER_NO: 1,
        PURCHASE_ORDER_NO: 2,
        ACCOUNT_NO: 3,
        CDF_NO: 4
    };

    static PurchaseOrderFilterItems = {
        ALL: ' ',
        CANCELED: 'D',
        COMPLETED: 'C',
        ARCHIEVE: 'A',
        OUTSTANDING: 'O',
        EXPIRED: 'E'

    };
    static AllComboLabel = '-- All --';

    static CustomerCreditLimitCallbackReturn = {
        EXCEEDED: 'credit-limit-exceeded',
    };

    static CustomerTypes = {
        CASH_BASIS: '2',
        DIRECT: '1',
        PROSPECT: '7',
        DEPOT_OR_SUPPLIER: '5',
        AGENT: '3',
        AGENT_CUSTOMER: '4',
        BRANCH: '6',
        DEALER_CUSTOMER: '9',
        DEALER: '8',
        DEPOT: '0'
    };

    static InvokeModes = {
        NEW: 0,
        MODIFY: 1,
        ENQUIRY: 2,
        ENQUIRY_HISTORY: 3,
        DELETE: 4,
        REUSE: 5,
        OTHER: 6,
    };

    static ProductSearchType = {
        COMMON_PRODUCTS: 0,
        SPECIAL_PRICE_ALL_PRODUCTS: 13,
        GENERAL_CREDIT_DEBIT_NOTE_PRODUCTS: 20
    };

    static Empty = ' ';

    static Remarks = {
        CLI_CONT_IDX_UP_0002: 'Index Code does not exist, Row will not processed',
        CLI_CONT_IDX_UP_0003: 'Index value does not exist, Row will not processed',
        CLI_CONT_IDX_UP_0004: 'Already Exists, Row will be updated with the new value',
        CLI_CONT_IDX_UP_0005: 'New Entry',
        CLI_CONT_IDX_UP_0006: 'Updated successfully',
        invalid_monthref: 'Invalid Month Ref',
        CLI_CONT_IDX_UP_0007: 'Formula No / Index Code does not exist, Row will not processed',
        CLI_CONT_IDX_UP_0008: 'Formula is not applied for Energy',
    };

    static FileRestrictions = {
        excel: '.xlsx'
    };

    static DifferByContracts = [
        { Text: 'YES', Value: 'Y' },
        { Text: 'NO', Value: 'N' }
    ];

    static GroupDataValues = {
        Category: 'K101',
        ApplicationCode: '02',
        BudgetGroup: '61'
    };

    static controlTypes = {
        string: 'string',
        object: 'object',
        number: 'number',
        date: 'date',
        boolean: 'boolean',
        other: 'other'
    };

    static ContractDropDownIds = {
        LocationId: 'LocationId', ProductTypeId: 'ProductTypeId',
        PvfDistributeMethod: 'PvfDistributeMethod', Responsibility: 'Responsibility', ContractType: 'ContractType'
    };

    static ContractFormControlerIds = { LocationId: 'LocationId', ProductTypeId: 'ProductTypeId', Code: 'Code' };

    static BooleanValues = {
        Y: 'Y',
        N: 'N',
        A: 'A'
    };

    static ContractFormBooleanControlerIds = {
        IsPrintEscalationLetter: 'IsPrintEscalationLetter',
        IsActive: 'IsActive',
        RollingIndexFlag: 'RollingIndexFlag'
    };

    static LanguageCode = {
        English: 'english',
        China: 'china'
    };

    static Locale = {
        Default: 'en-US',
        China: 'zh-CN',
        Australia: 'en-AU'
    };

    static DateFormat = {
        yyyyMMdd: 'yyyy/MM/dd',
        MMddyyyy: 'MM/dd/yyyy',
        DDmmyyyy: 'dd/MM/yyyy'
    };

    static TimeFormat = {
        HHmm: 'HH:mm'
    };

    static ContractCustomerInvokeMode = {
        Save: '0',
        Delete: '4',
        Deleted: '-4',
        Saved: '-1'
    };

    static ContractCustomerParentSearchFilter = {
        SearchType: 'G',
        Type: 'A',
        SearchCategory: '0',
        Status: 'Y,N'
    };

    static ContractPriceCustomerAppSearchFilter = {
        SearchOption: 0
    };

    static ranageMaxValue = 999999999;

    static RanagePriceMaxValue = 999999999999;

    static defaultDate = '1900/01/01';

    static CotractStatus = {
        Active: 'A'
    };

    static InvoiceSearchType = {
        INVOICE_ALL: 0,
        DEBIT_NOTE: 7,
        CREDIT_NOTE_WORKFLOW_INVOICE: 9,
        CONFIRMED_INVOICES_FOR_SALES_UPDATE: 5,
        UNCONFIRMED_INVOICES_FOR_SALES_UPDATE: 6,
    };

    static ReferenceType = {
        RECEIPT_REFERENCE: 'R',
        REQUIRED_DOCUMENT: 'D',
        INVOICE_REFERENCE: 'I',

    };

    static CountrySettingKeys = {
        ENABLE_MANUAL_DEBIT_NOTE_WORKFLOW: 'ENABLE_MANUAL_DEBIT_NOTE_WORKFLOW',
        DEBITNOTE_DUEDATE_FROM_INVOICE_AUTOSELECT: 'DEBITNOTE_DUEDATE_FROM_INVOICE_AUTOSELECT',
        CREATE_DEBITNOTE_ENABLE: 'CREATE_DEBITNOTE_ENABLE',
        AGREEMENT_BACKDATE_MONTHS: 'AGREEMENT_BACKDATE_MONTHS',
        APPROVED_EFFECTIVEDATE_REQUIRED: 'APPROVED_EFFECTIVEDATE_REQUIRED',
        CONTRACT_GP_APPROVAL_REQ: 'CONTRACT_GP_APPROVAL_REQ',
        CONTRACT_DP_APPROVAL_REQ: 'CONTRACT_DP_APPROVAL_REQ',
        CONTRACT_RP_APPROVAL_REQ: 'CONTRACT_RP_APPROVAL_REQ',
        CONTRACT_TP_APPROVAL_REQ: 'CONTRACT_TP_APPROVAL_REQ',
        CONTRACT_MC_APPROVAL_REQ: 'CONTRACT_MC_APPROVAL_REQ',
        CONTRACT_TC_APPROVAL_REQ: 'CONTRACT_TC_APPROVAL_REQ',
        CONTRACT_IP_APPROVAL_REQ: 'CONTRACT_IP_APPROVAL_REQ',
        DECIMAL_POINTS: 'DECIMAL_POINTS',
        REALTIME_CONTRACTPRICE_ESCALATION: 'REALTIME_CONTRACTPRICE_ESCALATION',
        APPROVAL_REQUESTER_COMMENTS: 'APPROVAL_REQUESTER_COMMENTS',
        LTCBS_CONSUMPTION_UPLOAD_APPROVAL_REQ: 'LTCBS_CONSUMPTION_UPLOAD_APPROVAL_REQ',
        CONTRACT_EVENTS_REQ: 'CONTRACT_EVENTS_REQ'
    };

    static CountrySettingValues = {
        ContractParadmInterfaceTrueValue: 'Y'
    };

    static ReceiptRowCallbackReturn = {
        VOID_RECEIPT: 'void-receipt',
    };

    static InvoiceGetMode = {
        DEFAULT: 0,
        UNMATCHED: 1,
        OVERDUE: 2,
        PROMISED_TO_PAY: 3,
        DISPUTED: 4
    };

    static Tax = {
        TAX_PERCENTAGE: 1,
        WITHHOLDING: 2,
        SPECIAL: 3,
        OTHER: 4
    };

    static TransactionRowCallbackReturn = {
        VOID_TRANSACTION: 'void-transactions',
    };

    static AgingBlocks = {
        DEFAULT: -1,
        TOTAL_DUE: 5
    };

    static DebitCreditNoteApprovalStatus = {
        APPROVED: 'A',
        NOT_APPROVED: 'N',
        REJECTED: 'R',
        CANCEL: 'C',
        INVALID: 'I',
        CREDIT_NOTE_GENERATED: 'G'
    };

    static TransactionEnquiryRowCallbackReturn = {
        TOTAL_MATCHED: 'fullyMatched',
        PARTIAL_MATCHED: 'PartialMatch',
    };

    static TransactionEnquiryType = {
        CreditJornal: 'CR',
        DebitJournal: 'DJ',
        TypeOfDocument: 'IJ',
        InvoiceRecordStatus: 'T',
        DocumentMatchStatus: 'Y',
        TransactionType: 'CJ',
        TransactinTypeDebit: 'DJ',
        GeneralLedgerAccountTypeE: 'E',
        GeneralLedgerAccountTypeR: 'R',
        JOURNAL: 'J',
        INVOICE: 'IN',
        INVOICE_REFERENCE: 'I',
        DEBIT_NOTE: 'DN'
    };

    static ReminderLetterMailCallbackReturn = {
        INVALID_EMAIL: 'invalid-email'
    };

    static ApprovalTypes = [
        'L',    //  MY PENDING
        'P',    //  ALL PENDING
        'R',    //  REJECTED
        'AllP', //  ALL APPROVED
        'A',    //  ALL
        'G'     //  CREDIT NOTE / DEBIT NOTE
    ];

    static ApprovalTypeCode = {
        CN: { value: 0 },
        DN: { value: 1 }
    };

    static ApprovalStatus = {
        APPROVED: 'Approved',
        PENDING: 'Pending',
        REJECTED: 'Rejected'
    };

    static RequestStatus = {
        GENERATED: 'G'
    };

    static InvoiceTypeCode = {
        DAILY_GAS: 'G',
        MONTHLY_RENTAL: 'R',
        DAILY_RENTAL: 'D',
        RENTAL: 'O',
        MONTHLY_GAS: 'M',
        TRANSFER: 'T',
        AGENT_INVOICE: 'F',
        AGREEMENT_INVOICE: 'A',
        DEBIT_NOTE_ONE: 'H',
        DEBIT_NOTE_TWO: 'I',
        DEBIT_NOTE_THREE: 'K',
        RENEWAL_INVOICE: 'L'
    };

    static InvoiceType = {
        DAILY_GAS: 'Gas (Daily)',
        MONTHLY_RENTAL: 'Monthly Rental',
        DAILY_RENTAL: 'Daily Rental',
        RENTAL: 'Rental (OMFL)',
        MONTHLY_GAS: 'Gas (Monthly)',
        TRANSFER: 'Transfer',
        AGENT_INVOICE: 'Agent Invoice',
        AGREEMENT_INVOICE: 'Agreement Invoice',
        DEBIT_NOTE_ONE: 'Debit Note',
        DEBIT_NOTE_TWO: 'Debit Note',
        DEBIT_NOTE_THREE: 'Debit Note',
        RENEWAL_INVOICE: 'Renewal Invoice',
        UN_KNOWN: 'Unknown'
    };

    static BankDeposit = {
        MODIFY: 'O',
        POST: 'P',
        ALL: 'ALL'
    };

    static GroupCategories = {
        ACTIONS: 'Q8',
        COUNTRY_NAME: '36',
        OWNERSHIP: '95',
        JOURNAL_REASON: '23',
        SHIPMENT_TYPES: 'MV',
        PVF_LETTER_DIST_METHODS: 'HC',
        RESPONSIBILITIES: 'HD',
        CONTRACT_TYPES: 'B1',
        CALCULATE_CUMULATIVE: 'L7',
        CATEGORY: 'K1',
        COMMODITY: 'K2',
        INDEX_FAMILY: 'K3',
        UNITS: 'K4',
        SUB_FORMULA: 'K1',
        JOURNAL_UNMATCH_REASON: 'Q6',
        GAS_FREQUENCY: 'J8',
        CALCULATED_START_DATE: 'MA',
        PRINT_FLAG: 'MB',
        PRICE_TYPE: 'NN',
        CONTRACT_PRODUCT_TYPE: 'O8',
        CONTRACT_PRESSURE_TYPE: 'HH',
        CONTRACT_FORMULAR_TYPE: 'HI',
        VOLUMETYPE: 'HF',
        FREQUENCY: 'J8',
        DEPARTMENTS: 'G5',
        UNMATCH_REASONS: 'Q6',
        REMINDER_LETTER_ROLES: 'R4',
        TRANSACTION_ENQUIRY_TYPE: 'ZY',
        AGREEMENT_TYPE: 'I2',
        SURRENDER_REASON: 'I7',
    };

    static ContractPriceSearchType = {
        All: 0,
        Gas: 1,
        Rental: 2,
        Delivery: 3,
        TakeOrPayPipeline: 4,
        TakeOrPayBulk: 5,
        MonthlyCharge: 6,
        TripCharge: 7,
        Immobilization: 8
    };

    static ProductType = {
        All: 0,
        Gas: 'GP'
    };

    static RequestType = {
        LTCBS_CONSUMPTION: 'LI'
    };

    static DeliveryMethod = {
        Pipeline: 'P',
        Bulk: 'B'
    };

    static PipelineQueryOption = {
        RangeSlabs: 0,
        TakeorPay: 1
    };

    static PriceType = {
        All: 'A',
        Contract: 'C',
        NonContract: 'N'
    };

    static VolumeType = {
        Actual: 'Actual'
    };


    static CreditNoteRequestStatus = {
        CANCELED: 'C',
        REJECTED: 'R',
        NOT_CREATED: 'N'
    };

    static DEFAULT_AGREEMENT_BACKDATE_MONTHS = '5';

    static AgreementType = {
        FREE: 'F',
        INVOICEABLE: 'R',
        NON_INVOICEABLE: 'N',
        CTS: 'C'
    };

    static InvoiceSalesUpdateStatus = {
        CONFIRMED: 'B',
        NOT_CONFIRMED: 'N'
    };
}

