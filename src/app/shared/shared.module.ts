import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseComponent } from './base/base.component';

import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { DialogsModule } from '@progress/kendo-angular-dialog';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { LabelModule } from '@progress/kendo-angular-label';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { PopupModule } from '@progress/kendo-angular-popup';
import { TooltipModule } from '@progress/kendo-angular-tooltip';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { GridModule } from '@progress/kendo-angular-grid';
import { ActionPanelComponent } from './components/action-panel/action-panel.component';
import { ChartsModule } from '@progress/kendo-angular-charts';

export const sharedModuleComponents = [
  BaseComponent,
  ActionPanelComponent,
];

@NgModule({
  declarations: [
    ...sharedModuleComponents
  ],
  imports: [
    CommonModule,
    FormsModule,
    DateInputsModule,
    DialogsModule,
    DropDownsModule,
    InputsModule,
    LabelModule, 
    LayoutModule,
    PopupModule,
    TooltipModule,
    ButtonsModule,
    GridModule,
    ChartsModule
  ],
  exports: [
    DateInputsModule,
    DialogsModule,
    DropDownsModule,
    InputsModule,
    LabelModule,
    LayoutModule,
    PopupModule,
    TooltipModule,
    CommonModule,
    ButtonsModule,
    GridModule,
    TranslateModule,
    ChartsModule,
    ...sharedModuleComponents
  ]
})
export class SharedModule { }
