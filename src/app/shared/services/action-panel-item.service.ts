import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

// Services

// Models

// Constants
import { Constant } from '../utility/constant';
import { ActionPanelItem } from '../models/action-panel-item.model';

@Injectable()
export class ActionPanelItemService {

  private actionPanelItems: Array<ActionPanelItem>;
  private actionPanelItem: ActionPanelItem;

  constructor() {
    this.actionPanelItems = new Array<ActionPanelItem>();
    this.generateActionPanelItemList();
  }

  getActionalPanelList(actionPanelItemLinkedWith: string): Observable<Array<ActionPanelItem>> {
    let selectedActionPanelItems: Array<ActionPanelItem>;
    selectedActionPanelItems = new Array<ActionPanelItem>();

    if (this.actionPanelItems !== undefined
      && this.actionPanelItems.length > 0) {
      this.actionPanelItems.forEach(element => {
        // if (element.ActionPanelItemLinkedWith.indexOf(actionPanelItemLinkedWith) >= 0)
          //  {
          selectedActionPanelItems.push(element);
        // }
      });
    }

    return of(selectedActionPanelItems);
  }

  enableActionPanelButtons(actionPanelItems: ActionPanelItem[], buttons: string[] = []): void {
    actionPanelItems.forEach((actionPanelItem, i) => {
        if (buttons && buttons.length === 0) {
          // Dsable All
          actionPanelItem.Disabled = false;
        } else {
          // Dsable Specific
          let foundButton: string = buttons.find((button) => {
            return button === actionPanelItem.ActionPanelItemKey;
          });
          if (foundButton) {
            actionPanelItem.Disabled = false;
          }
          foundButton = '';
        }
    });
  }

  disableActionPanelButtons(actionPanelItems: ActionPanelItem[], buttons: string[] = []): void {
    actionPanelItems.forEach((actionPanelItem, i) => {
        if (buttons && buttons.length === 0) {
          // Dsable All
          actionPanelItem.Disabled = true;
        } else {
          // Dsable Specific
          let foundButton: string = buttons.find((button) => {
            return button === actionPanelItem.ActionPanelItemKey;
          });
          if (foundButton) {
            actionPanelItem.Disabled = true;
          }
          foundButton = '';
        }
    });
  }

  private generateActionPanelItemList(): void {
    this.addAgreementRelatedItems();
  }

  private addAgreementRelatedItems(): void {
    this.actionPanelItem = new ActionPanelItem();
    this.actionPanelItem.ActionPanelItemKey = Constant.ActionPanelItemKey.KEY_AGREEMENT_CREATE;
    this.actionPanelItem.ActionPanelItemLinkedWith.push(Constant.ActionOrWidgetLinkedWith.ACCESSIBILITY);
    this.actionPanelItem.FunctionCode = Constant.FunctionCode.FC_MNU_AGREEMENT_CREATE;
    this.actionPanelItem.Description = 'Create Agreement';
    this.actionPanelItem.Disabled = false;
    this.actionPanelItem.Icon = Constant.Icon.IC_AGREEMENT_CREATE;
    this.actionPanelItems.push(this.actionPanelItem);

    this.actionPanelItem = new ActionPanelItem();
    this.actionPanelItem.ActionPanelItemKey = Constant.ActionPanelItemKey.KEY_AGREEMENT_CREATE;
    this.actionPanelItem.ActionPanelItemLinkedWith.push(Constant.ActionOrWidgetLinkedWith.ACCESSIBILITY);
    this.actionPanelItem.FunctionCode = Constant.FunctionCode.FC_MNU_AGREEMENT_CREATE;
    this.actionPanelItem.Description = 'Modify Agreement';
    this.actionPanelItem.Disabled = false;
    this.actionPanelItem.Icon = Constant.Icon.IC_AGREEMENT_CREATE;
    this.actionPanelItems.push(this.actionPanelItem);
  }

}
