export const RouteConstant = {
    CONFIGURATION_STANDARD: 'configuration/standard-configuration',
    CONFIGURATION_OBC: 'configuration/obc-configuration',

    RESOURCES_LOGISTICS: 'resources/logistics',
    RESOURCES_INSTALLATION: 'resources/installation',
    RESOURCES_SOURCES: 'resources/sources',

    FAST_TRIP: 'ftc/fast-trip',
    TRIP_SCHEDULER:'trip-scheduler/schedule'
};
