import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';

import { RouteManagerService } from 'src/app/core/services/route-manager.service';
import { TabFormViewModel } from '../models/common/tab-form-view.model';
import { ActionPanelItem } from '../models/common/action-panel-item.model';

@Component({
  template: '<div></div>'
})
export class BaseComponent<T extends TabFormViewModel> implements OnInit, OnDestroy, AfterViewInit {
  Title: string;
  MyTabId: number;
  IsLoading = true;
  formObj: any;
  ActionPanelItems: Array<ActionPanelItem> = [];  
  FormViewModel: T;
  private sub: any;

  constructor(public routeManagerService: RouteManagerService,
              public router: Router,
              private child: TabFormViewModel) {
    this.MyTabId = 0;
    this.FormViewModel =  this.child as T;
    this.sub = this.router.events.subscribe(e => {
      if (e instanceof NavigationStart) {
        this.putToBundle();
      } else if (e instanceof NavigationEnd) {
        this.MyTabId = this.routeManagerService.selectedTab.id;
        this.getFromBundle();
      }
    });
  }

  ngOnInit() {
    this.MyTabId = this.routeManagerService.selectedTab.id;
    this.getFromBundle(); // Get the to be displayed in the tab (This is mainly for the tab navigation)
    setTimeout(() => {
      this.IsLoading = false;
      this.routeManagerService.setTitle(this.MyTabId, this.Title);
    }, 100);
    this.moduleOnInit();
  }

  ngAfterViewInit() {
    // console.log('base ngAfterViewInit ' + new Date());
    // this.isShowFilterPanel = false;
    // this.putToBundle(); // ToCheck-> When IL is changing as current tab data in not in bundle
    // (Unless user change the tab), this was called. But still at onDestroy this is calling as user can change the selections.
    this.moduleAfterViewInit();
  }

  ngOnDestroy() {
    this.putToBundle();
    this.moduleOnDestroy();
    this.sub.unsubscribe();
  }


  moduleOnInit(): void { }

  moduleAfterViewInit(): void { }

  moduleOnDestroy(): void { }

  getFromBundle(): void {
    // For Bundle Array
    this.routeManagerService.setWorkingTab(this.MyTabId);
    if (this.routeManagerService.workingTab != null) {
      if (this.routeManagerService.workingTab.bundleArray != null && this.routeManagerService.workingTab.bundleArray.length > 0) {
        // let me = this;
        Object.values(this.routeManagerService.workingTab.bundleArray).forEach((element) => {
          // let _me = me;
          // _me[element[0]] = element[1];
          this[element[0]] = element[1];
        });
      }
    }

    // For Bundle Object
    this.routeManagerService.setWorkingTab(this.MyTabId);
    if (this.routeManagerService.workingTab != null) {
      if (this.routeManagerService.workingTab.bundle != null) {
        this['formObj'] = this.routeManagerService.workingTab.bundle;
      }
    }
  }

  putToBundle(): void {
    this.routeManagerService.setWorkingTab(this.MyTabId);
    if (this.routeManagerService.workingTab != null) {
      this.resetSearchObjects();
      this.routeManagerService.workingTab.bundle = this.formObj;
    }
  }

  resetSearchObjects(): void { }

  closePage(): void {
    this.routeManagerService.deleteTab(this.MyTabId);
  }
}
