import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';

import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { AppComponent } from './app.component';
import { AppTopBarComponent } from './layout/app-top-bar/app-top-bar.component';
import { AppMenuComponent } from './layout/app-menu/app-menu.component';
import { AppSubMenuComponent } from './layout/app-sub-menu/app-sub-menu.component';
import { ScrollPanelComponent } from './layout/scroll-panel/scroll-panel.component';
import { AppRightPanelComponent } from './layout/app-right-panel/app-right-panel.component';
import { AppFooterComponent } from './layout/app-footer/app-footer.component';
import { MockHttpRequestInterceptor } from './core/interceptors/mock-http-request-interceptor';
import { HttpRequestInterceptor } from './core/interceptors/http-request-interceptor';
import { GlobalVariable } from './shared/globals';
export function TranslationLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    AppMenuComponent,
    AppSubMenuComponent,
    AppTopBarComponent,
    AppFooterComponent,
    ScrollPanelComponent,
    AppRightPanelComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    CoreModule,
    SharedModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (TranslationLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: GlobalVariable.UseMock ? MockHttpRequestInterceptor : HttpRequestInterceptor, multi: true },
    TranslateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
