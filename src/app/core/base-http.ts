import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { BlacException } from '../shared/models/common/blac-exception.model';
import { Extentions } from '../shared/utility/extentions';
import { GlobalVariable } from '../shared/globals';
import { environment } from 'src/environments/environment';



export class Param {
    public ey: string;
    public value: string;
}

export class BaseHttp {
    Params: HttpParams;
    HttpOptions: any;

    constructor(protected http: HttpClient) {

    }

    GetParams(searchObj: any): HttpParams {
        this.Params = new HttpParams();
        // for (const key of Object.keys(searchObj)) {
        //     this.params.set(key, searchObj[key].toString());
        // }
        Object.keys(searchObj).forEach((key) => {
            if (Extentions.IsDate(searchObj[key])) {
                this.Params = this.Params.append(key, Extentions.ToValidDateTime(searchObj[key]));
                // this.params = this.params.append(key, searchObj[key]);
            } else {
                this.Params = this.Params.append(key, searchObj[key]);
            }
        });
        return this.Params;
    }


    HttpGet<T>(methodName: string, httpParams?: any): Observable<T | BlacException> {
        if (httpParams) {
            this.Params = this.GetParams(httpParams);
            return this.http.get<T>(this.getUrl(methodName), { params: this.Params })
                .pipe(
                    catchError(err => this.handleHttpError(err))
                );
        } else {
            return this.http.get<T>(this.getUrl(methodName), { withCredentials: true, headers: this.getHeaders() })
                .pipe(
                    catchError(err => this.handleHttpError(err))
                );
        }
    }

    HttpGetFile<T>(methodName: string, httpParams?: any): Observable<T | BlacException> {
        if (httpParams) {
            this.Params = this.GetParams(httpParams);
            return this.http.get<T>(this.getUrl(methodName), { params: this.Params, responseType: 'blob' as 'json' })
                .pipe(
                    catchError(err => this.handleHttpError(err))
                );
        } else {
            return this.http.get<T>(this.getUrl(methodName))
                .pipe(
                    catchError(err => this.handleHttpError(err))
                );
        }
    }

    HttpPost<T>(methodName: string, object?: any): Observable<T | BlacException> {
        return this.http.post<T>(this.getUrl(methodName), object)
            .pipe(
                catchError(err => this.handleHttpError(err))
            );
    }

    HttpPostFile<T>(methodName: string, object: any): Observable<T | BlacException> {
        return this.http.post<T>(this.getUrl(methodName), object, { headers: new HttpHeaders() })
            .pipe(
                catchError(err => this.handleHttpError(err))
            );
    }

    HttpPostFileDownload(methodName: string, object: any): Observable<Blob | BlacException> {
        return this.http.post(this.getUrl(methodName), object, { responseType: 'blob' })
            .pipe(
                catchError(err => this.handleHttpError(err))
            );
    }

    HttpPut<T>(methodName: string, object: any): Observable<T | BlacException> {
        return this.http.put<T>(this.getUrl(methodName), object)
            .pipe(
                catchError(err => this.handleHttpError(err))
            );
    }

    HttpDelete<T>(methodName: string): Observable<T | BlacException> {
        return this.http.delete<T>(this.getUrl(methodName))
            .pipe(
                catchError(err => this.handleHttpError(err))
            );
    }

    HttpDeleteFile<T>(methodName: string, object: any): Observable<T | BlacException> {
        return this.http.request<T>('delete', methodName, { body: object }).pipe(
            catchError(err => this.handleHttpError(err))
        );
    }

    private handleHttpError(error: HttpErrorResponse): Observable<BlacException> {
        let blacException = new BlacException();
        blacException.ErrorCode = error.status;
        blacException.Message = error.statusText;
        blacException.ErrorMessage = error.error;
        return throwError(blacException);
    }

    private getUrl(url: string): string {
        if (GlobalVariable.UseMock) {
            switch (url) {
                case 'Auth':
                    return GlobalVariable.Apibase + '' + 'assets/api/auth-data.json';

                default:
                    return url;
            }
        };

        return url;
    }

    private getHeaders() {
        const headers = new HttpHeaders();
        headers.append('Access-Control-Allow-Origin', GlobalVariable.Apibase);
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Credentials', 'true');
        headers.append('Accept', 'application/json');
        headers.append('Origin', GlobalVariable.Apibase);

        return headers;
    }
}
