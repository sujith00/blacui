import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouteManagerService } from './services/route-manager.service';
import { CanActivateGuard } from './services/can-activate.guard';
import { RouteGuardService } from './services/route-guard.service';
import { AppHttpInterceptor } from './services/http-interceptor.service';
import { LoaderService } from './services/loader.service';
import { ActionPanelItemService } from '../shared/services/action-panel-item.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    RouteManagerService,
    CanActivateGuard,
    RouteGuardService,
    AppHttpInterceptor,
    LoaderService,
    ActionPanelItemService
  ]
})
export class CoreModule { }
