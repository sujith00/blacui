import { Injectable } from '@angular/core';
import { CanActivateChild, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { RouteManagerService } from './route-manager.service';

@Injectable()
export class RouteGuardService implements CanActivateChild {

  constructor(private router: Router,
              private routeManagerService: RouteManagerService) { }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canNavigate(state.url);
  }

  canNavigate(url: string): boolean {
    // if (this.authService.IsLoggedIn) {
    //   if (this.routeManagerService != null) {
    //     return this.routeManagerService.checkAndNavigate(url);
    //   }
    //   return false;
    // } else {
    //   this.router.navigate(['/login']);
    //   return false;
    // }
    return this.routeManagerService.checkAndNavigate(url);
  }
}
