import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS, HttpClient, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { AppHttpInterceptor } from './http-interceptor.service';
import { LoaderService } from './loader.service';

describe('HttpInterceptorService', () => {
  // let loaderService: LoaderService;
  // let httpMock: HttpTestingController;
  // let httpClient: HttpClient;
  // let someResponse: any;
  // let errResponse: any;

  // beforeEach(() => {
  //     TestBed.configureTestingModule({
  //         imports: [HttpClientTestingModule],
  //         providers: [
  //             {
  //                 provide: HTTP_INTERCEPTORS,
  //                 useClass: AppHttpInterceptor,
  //                 multi: true
  //             },
  //             LoaderService
  //         ]
  //     });

  //     loaderService = TestBed.get(LoaderService);
  //     httpMock = TestBed.get(HttpTestingController);
  //     httpClient = TestBed.get(HttpClient);
  // });


  // it('should contain the header value \'Content-Type\' when making a server call', () => {
  //     httpClient.get('/data').subscribe(
  //         response => {
  //             expect(response).toBeTruthy();
  //             expect(loaderService.asycTackCount).toBe(0);
  //         }
  //     );
  //     const req = httpMock.expectOne(reqest => reqest.headers.has('Content-Type'));
  //     expect(req.request.method).toEqual('GET');
  //     req.flush({ hello: 'world' });
  // });


  // it('should increment the \'asyncTaskCount\' when a call is made', () => {
  //     httpClient.get('/data').subscribe(() => {});
  //     const req = httpMock.expectOne('/data');
  //     expect(loaderService.asycTackCount).toBe(1);
  //     req.flush({ hello: 'world' });
  // });


  // it('should decrement the \'asyncTaskCount\' when a call is finished', () => {
  //     httpClient.get('/data').subscribe(() => {});
  //     const req = httpMock.expectOne('/data');
  //     req.flush({ hello: 'world' });
  //     expect(loaderService.asycTackCount).toBe(0);
  // });


  // it('should decrement the \'asyncTaskCount\' when an error is occured', () => {
  //     loaderService.asycTackCount = 1;
  //     httpClient.get('/error').subscribe(() => {}, () => {});
  //     httpMock.expectOne('/error').error(new ErrorEvent('Unauthorized error'), { status: 401 });
  //     expect(loaderService.asycTackCount).toBe(1);
  // });


  // it('should capture the error if unable to reach the server or any network issues', () => {
  //     httpClient.get('/error').subscribe((response: any) => { }, (errorResponse) => {
  //         errResponse = errorResponse;
  //     });
  //     httpMock.expectOne('/error').error(new ErrorEvent('Unknown Error'));
  //     expect(errResponse.statusText).toBe('Unknown Error');
  // });


  // it('should capture the error(s) thrown by the application\'s backend services', () => {
  //     // Refer https://stackoverflow.com/questions/46028804/how-to-mock-angular-4-3-httpclient-an-error-response-in-testing
  //     const mockErrorResponse = {
  //         status: 404, statusText: 'Bad Request'
  //     };
  //     const data = 'Invalid request parameters';
  //     httpClient.get('/error').subscribe(res => someResponse = res, err => errResponse = err);
  //     httpMock.expectOne('/error').flush(data, mockErrorResponse);
  //     expect(errResponse.error).toBe(data);
  // });


  // afterEach(() => {
  //     httpMock.verify();
  // });
});
