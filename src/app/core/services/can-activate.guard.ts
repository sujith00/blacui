import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { RouteManagerService } from './route-manager.service';

@Injectable()
export class CanActivateGuard implements CanActivate {
  constructor(private routeManagerService: RouteManagerService){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.routeManagerService.checkAndNavigate(state.url);
  }
}
