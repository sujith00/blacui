import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Tab } from 'src/app/shared/models/common/tab.model';

@Injectable()
export class RouteManagerService {
  private nextTabId = 0;
  private currectUrlParts: string[];
  private selectedUrlParts: string[];
  private bundle: any = null;

  public router: Router;
  public selectedTabIndex = 0;
  public tabs: Tab[];
  public currentUrl: string;
  public selectedTab: Tab;
  public workingTab: Tab;
  public isInitialRoute: boolean;
  public isNavigationFromTab: boolean;
  public canAddNewTab = true;
  public isChildRoute = false;


  constructor(router: Router) {
      this.tabs = new Array();
      this.currentUrl = '';
      this.isNavigationFromTab = false;
      this.router = router;
  }

  checkAndNavigate(url: string): boolean {
      this.isChildRoute = true;
      if (url === '/main') {
          this.tabs = new Array<Tab>();
          this.selectedTab = null;
          this.isChildRoute = false;
          return true;
      } else if (this.isActiveByUrl(url)) {
          return false;
      } else if (this.isRouteAlreadyExist(url)) {
          this.selectTabByUrl(url);
          return true;
      } else {
          this.addTab();
          this.addCurrentUrl(url);
          return true;
      }
  }

  navigateToComponent(tabId: number): void {
      let url = '';
      for (let i = 0; i < this.tabs.length; i++) {
          if (this.tabs[i].id === tabId) {
              url = this.tabs[i].url;
              break;
          }
      }
      if (url !== '') {
          this.router.navigate([url]);
      }
  }

  isFromAndToComponentSame() {
      this.currectUrlParts = this.currentUrl.split('/');
      this.selectedUrlParts = this.selectedTab.url.split('/');
      if (this.currectUrlParts[3] === this.selectedUrlParts[3]) {
          return true;
      }
  }

  selectTabByUrl(url: string): boolean {
      if (this.tabs != null && this.tabs.length > 0) {
          for (let index = 0; index < this.tabs.length; index++) {
              if (this.tabs[index].url === url) {
                  this.selectedTabIndex = index;
                  this.selectedTab = this.tabs[index];
                  break;
              }
          }
          return true;
      }
      return false;
  }

  selectTab(tabId: number): boolean {
      if (this.tabs != null && this.tabs.length > 0) {
          for (let i = 0; i < this.tabs.length; i++) {
              if (this.tabs[i].id === tabId) {
                  this.selectedTabIndex = i;
                  this.selectedTab = this.tabs[i];
                  break;
              }
          }
          return true;
      } else {
          this.selectedTabIndex = -1;
          return false;
      }
  }

  addTab(): void {
      if (this.tabs == null) {
          this.tabs = new Array<Tab>();
      }
      this.setNextTabId();
      this.tabs.push({
          id: this.nextTabId,
          title: this.currentUrl,
          url: this.currentUrl,
          bundle: this.bundle,
          bundleArray: new Array()
      });
      this.selectedTabIndex = this.tabs.length - 1;
      this.selectedTab = this.tabs[this.selectedTabIndex];
  }

  addCurrentUrl(url: string): void {
      if (this.tabs != null && this.tabs.length > 0) {
          this.currentUrl = url;
          this.tabs[this.tabs.length - 1].url = url;
      }
  }

  deleteTab(tabId: number): void {
      let index: number;
      if (this.tabs != null && this.tabs.length > 0) {
          for (let i = 0; i < this.tabs.length; i++) {
              if (this.tabs[i].id === tabId) {
                  index = i;
                  break;
              }
          }
          this.tabs.splice(index, 1);
          if (this.tabs != null && this.tabs.length > 0) {
              if (index === this.tabs.length) {
                  index--;
              }
          } else {
              this.selectedTabIndex = -1;
              this.currentUrl = '';
          }
          if (this.tabs.length === 0) {
              // this.router.navigate(['/main']);
              this.selectedTab = null;
          } else {
              this.navigateToComponent(this.tabs[index].id);
          }
      }
  }

  isActiveByUrl(url: string): boolean {
      if (this.selectedTab !== null && this.selectedTab !== undefined && url === this.selectedTab.url) {
          return true;
      }
      return false;
  }

  isActive(tabid: number): boolean {
      if (tabid === this.selectedTab.id) {
          return true;
      }
      return false;
  }

  isRouteAlreadyExist(url: string): boolean {
      if (this.tabs != null && this.tabs.length > 0) {
          for (let i = 0; i < this.tabs.length; i++) {
              if (this.tabs[i].url === url) {
                  return true;
              }
          }
      }
      return false;
  }

  setTitle(tabId: number, title: string): void {
      if (this.tabs != null && this.tabs.length > 0) {
          for (let i = 0; i < this.tabs.length; i++) {
              if (this.tabs[i].id === tabId) {
                  this.tabs[i].title = title;
                  break;
              }
          }
      }
  }

  setWorkingTab(tabId: number): void {
      this.workingTab = null;
      if (this.tabs != null && this.tabs.length > 0) {
          for (let i = 0; i < this.tabs.length; i++) {
              if (this.tabs[i].id === tabId) {
                  this.workingTab = this.tabs[i];
                  break;
              }
          }
      }
  }

  private setNextTabId() {
      if (this.nextTabId === 0) {
          this.nextTabId = 1;
      } else {
          if (this.tabs != null && this.tabs.length > 0) {
              this.nextTabId = this.tabs[this.tabs.length - 1].id + 1;
          } else {
              this.nextTabId = 1;
          }
      }
  }
}
