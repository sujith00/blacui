import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { map, catchError, finalize } from 'rxjs/operators';

import mockLookup from "src/app/mock/mock-json-lookup.json";
import { StringHelper } from 'src/app/shared/utility/string-helper';
//import { MockAccesibilityService } from 'src/app/mock/services/mock-accessibility.service';
@Injectable()
export class MockHttpRequestInterceptor implements HttpInterceptor {

    constructor(private injector: Injector) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        for (const element of mockLookup.data) {
            const jsonUrl = encodeURI(element.url);
            if (request.url.indexOf(jsonUrl) > -1) {
                let jsonData: any;
                let message: string;
                if (element.jsonFile.indexOf('.json') > -1) {                    
                    jsonData = require( 'src/app/mock/' + element.jsonFile);
                    message = StringHelper.Concat('Loading ', element.url, ' from local json.');
                } 
                // else {
                //     var myService = this.injector.get('MockAccesibilityService');
                //     message = StringHelper.Concat('Loading ', element.url, ' from mock service.');
                //     jsonData = myService.getData();
                // }
                console.log(message);
                return of(new HttpResponse({ status: 200, body: jsonData }));
            }
        }
        console.log(StringHelper.Concat('Mock data not found! ', request.url));
        return next.handle(request)
            .do((event: HttpEvent<any>) => {
                return event['body'];
            })
            .catch((execption) => {
                return Observable.throw(execption.error);
            }) as any;
    }
}
