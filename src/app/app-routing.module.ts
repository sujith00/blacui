import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RouteGuardService } from './core/services/route-guard.service';
import { CanActivateGuard } from './core/services/can-activate.guard';

const routes: Routes = [  
  {
    path: 'configuration',
    data: { preload: false },
    loadChildren: () => import('./configuration/configuration.module').then(m => m.ConfigurationModule),
    canActivateChild: [RouteGuardService]
  },{
    path:'ftc', 
    loadChildren: () => import('./ftc/ftc.module').then(m => m.FastTripModule),
    canActivateChild: [RouteGuardService]
  },
  {
    path:'trip-scheduler', 
    loadChildren: () => import('./trip-scheduler/trip-scheduler.module').then(m => m.TripSchedulerModule),
    canActivateChild: [RouteGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})

export class AppRoutingModule { }
